# WaifuTaggerLoopback

## Description
A basic script to use the [Waifu Diffusion 1.4 Tagger](https://github.com/toriato/stable-diffusion-webui-wd14-tagger) to generate prompts for a result image in img2img and feed those prompts to itself in a loop.

## Installation
Add the tag2img.py file to `scripts/`

## License
TBD
